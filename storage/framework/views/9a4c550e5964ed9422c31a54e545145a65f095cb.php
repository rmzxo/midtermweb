</!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width">
	<title><?php echo $__env->yieldContent('title'); ?></title>
	<link rel="stylesheet" href="<?php echo e(asset('public/css/app.css')); ?>"> 
</head>
<body>
	
	<?php echo $__env->yieldContent('content'); ?>

</body>
</html>