<?php $__env->startSection('title', 'Добавить'); ?>

<?php $__env->startSection('content'); ?>

	<div class="col-md-7">
	
	<?php echo Form::open(['route' => 'make.store']); ?>

    <div class="form-group">
    	<div class="col-md-3">
    	  <?php echo e(Form::label('title', 'Заголовок')); ?>

    	</div>
    	<div class="col-md-9">
          <?php echo e(Form::text('title', null, ['class' => 'form-control'])); ?>

        </div>
    </div>


    <div class="form-group">
    	<div class="col-md-3">
    	  <?php echo e(Form::label('description', 'Текст')); ?>

    	</div>
    	<div class="col-md-9">
          <?php echo e(Form::textarea('description', null, ['class' => 'form-control'])); ?>

        </div>
    </div>


    <div class="form-group">
    	<div class="col-md-9 col-md-offset-3">
          <?php echo e(Form::submit('Сохранить', ['class' => 'btn btn-primary'])); ?>

        </div>
    </div>


	<?php echo Form::close(); ?>

</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.index', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>