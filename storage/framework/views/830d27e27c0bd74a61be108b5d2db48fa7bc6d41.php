<?php $__env->startSection('title','Просмотр'); ?>

<?php $__env->startSection('content'); ?>
	
	<h1><?php echo e($post->title); ?></h1>
	<p><?php echo $post->description; ?></p>

	<a href="<?php echo e(URL::to('make/' . $post->id) . '/edit'); ?>" class="btn btn-default">Редактировать</a>

	<?php echo Form::open(['method' => 'DELETE', 'route' => ['make.destroy', $post->id]]); ?>

	<?php echo Form::submit('Удалить', ['class' => 'btn btn-danger']); ?>

	<?php echo Form::close(); ?>



<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.index', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>