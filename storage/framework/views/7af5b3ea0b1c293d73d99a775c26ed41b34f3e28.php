<?php $__env->startSection('title', 'Все посты'); ?>

<?php $__env->startSection('content'); ?>
	<div class="bs-example" data-example-id="striped-table">
	<table class="table table-striped">
	 <thead>
	  <tr>
	    <th>ID</th>
	    <th>Заголовок</th>
	    <th>Текст</th>
	    <th style="width: 260px">Управление</th>
	  </tr> 
	 </thead> 
	   <tbody>
	<?php $__currentLoopData = $post; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $p): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
		<tr>
		 <th scope="row"><?php echo e($p->id); ?></th>
		 <td><?php echo e($p->title); ?></td>
		 <td><?php echo $p->description; ?></td>
		 <td style="width: 260px">
		 	
		 	<a href="<?php echo e(URL::to('make/' . $p->id) . '/edit'); ?>" class="btn btn-default" style="float: left">Редактировать</a>

	<?php echo Form::open(['method' => 'DELETE', 'route' => ['make.destroy', $p->id]]); ?>

	<?php echo Form::submit('Удалить', ['class' => 'btn btn-danger']); ?>

	<?php echo Form::close(); ?>


		 </td>
		</tr>
	<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>		 
	    </tbody>
	</table>
	 </div>

	<?php echo e($post->links()); ?> 

<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.index', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>