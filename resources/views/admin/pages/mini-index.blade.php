@extends('admin.index')

@section('title', 'Все посты')

@section('content')
	<div class="bs-example" data-example-id="striped-table">
	<table class="table table-striped">
	 <thead>
	  <tr>
	    <th>ID</th>
	    <th>Заголовок</th>
	    <th>Текст</th>
	    <th style="width: 260px">Управление</th>
	  </tr> 
	 </thead> 
	   <tbody>
	@foreach($post as $p)
		<tr>
		 <th scope="row">{{ $p->id }}</th>
		 <td>{{ $p->title }}</td>
		 <td>{!! $p->description !!}</td>
		 <td style="width: 260px">
		 	
		 	<a href="{{ URL::to('make/' . $p->id) . '/edit' }}" class="btn btn-default" style="float: left">Редактировать</a>

	{!! Form::open(['method' => 'DELETE', 'route' => ['make.destroy', $p->id]]) !!}
	{!! Form::submit('Удалить', ['class' => 'btn btn-danger']) !!}
	{!! Form::close() !!}

		 </td>
		</tr>
	@endforeach		 
	    </tbody>
	</table>
	 </div>

	{{$post->links()}} 

@endsection