@extends('admin.index')

@section('title','Просмотр')

@section('content')
	
	<h1>{{ $post->title }}</h1>
	<p>{!! $post->description !!}</p>

	<a href="{{ URL::to('make/' . $post->id) . '/edit' }}" class="btn btn-default">Редактировать</a>

	{!! Form::open(['method' => 'DELETE', 'route' => ['make.destroy', $post->id]]) !!}
	{!! Form::submit('Удалить', ['class' => 'btn btn-danger']) !!}
	{!! Form::close() !!}


@endsection