@extends('admin.index')

@section('title', 'Редактировать')

@section('content')

	<div class="col-md-7">
	
    {!! Form::model($post, array('route' => array('make.update', $post->id), 'method' => 'PUT')) !!}
    <div class="form-group">
    	<div class="col-md-3">
    	  {{ Form::label('title', 'Заголовок') }}
    	</div>
    	<div class="col-md-9">
          {{ Form::text('title', null, ['class' => 'form-control']) }}
        </div>
    </div>


    <div class="form-group">
    	<div class="col-md-3">
    	  {{ Form::label('description', 'Текст') }}
    	</div>
    	<div class="col-md-9">
          {{ Form::textarea('description', null, ['class' => 'form-control']) }}
        </div>
    </div>


    <div class="form-group">
    	<div class="col-md-9 col-md-offset-3">
          {{ Form::submit('Сохранить', ['class' => 'btn btn-primary']) }}
        </div>
    </div>


	{!! Form::close() !!}
</div>
@endsection