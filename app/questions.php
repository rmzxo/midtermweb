<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class questions extends Model
{
    public $table='Question';
    public $timestamps = false;
}
