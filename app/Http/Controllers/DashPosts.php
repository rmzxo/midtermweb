<?php

namespace App\Http\Controllers;
use App\questions;
use Illuminate\Http\Request;

class DashPosts extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         $post = questions::orderby('created_at', 'asc')->paginate(3);

         return view('admin.pages.mini-index')->withPost($post);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.pages.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $post = new questions();

        $post->title = $request->title;
        $post->description = $request->description;

        $post->save();

        $request->session()->flash('success', 'Запись сохранена!');


        return redirect()->route('make.show', $post->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post = questions::find($id);

        return view('admin.pages.show')->withPost($post);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $post = questions::find($id);

        return view('admin.pages.edit')->withPost($post);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $post = questions::find($id);

        $post->title = $request->title;
        $post->description = $request->description;

        $post->save();

        $request->session()->flash('success', 'Успешное редактирование!');


        return redirect()->route('make.show', $post->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = questions::find($id);

        $post->delete();

        return view('admin.pages.mini-index');
    }
}
